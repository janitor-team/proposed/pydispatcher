Source: pydispatcher
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python3-all,
               python3-setuptools
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Neil Muller <drnlmuller+debian@gmail.com>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/python-team/modules/pydispatcher.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/pydispatcher
Homepage: http://pydispatcher.sourceforge.net/
Testsuite: autopkgtest-pkg-python

Package: python3-pydispatch
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-pydispatch-doc
Description: Python 3 signal dispatching mechanism
 PyDispatcher provides the Python programmer with a
 multiple-producer-multiple-consumer signal-registration and routing
 infrastructure for use in multiple contexts. The mechanism of PyDispatcher
 started life as a highly rated recipe in the Python Cookbook. The project
 aims to include various enhancements to the recipe developed during use in
 various applications.
 .
 This package contains the Python 3 version of PyDispatcher.

Package: python-pydispatch-doc
Architecture: all
Depends: ${misc:Depends}
Section: doc
Description: documentation for python3-pydispatch
 PyDispatcher provides the Python programmer with a
 multiple-producer-multiple-consumer signal-registration and routing
 infrastructure for use in multiple contexts. The mechanism of PyDispatcher
 started life as a highly rated recipe in the Python Cookbook. The project
 aims to include various enhancements to the recipe developed during use in
 various applications.
 .
 This package contains the documentation for PyDispatcher. It covers
 the Python 3 versions.
